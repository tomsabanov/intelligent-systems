library(caret)
library(dplyr)
library(GA)
library(parallel)

# Import the data from the csv
mydata <- read.csv("./DLBCL.csv",
                   header=TRUE,
                   sep=",")

# The X argument is definitely pointless as it just enumerates the
# rows. We aren't sure what the attribute 'atclass' is, but it's 
# name doesn't match the scheme used for other attributes, so we
# will presume it is not important as well.
mydata$X = NULL
mydata$atclass = NULL

# Use 3 fold cross validation
trn_ctrl = trainControl(method = 'cv', number = 3)

# A weight function for the fitness function, that prefers lower
# values of x and reaches the minimum at x = 1000
w <- function(x)
{
  w = 1 + 400 / (1 + exp(0.01 * (x - 500))) + 400 / (1 + exp(0.01 * x))
}

# The fitness function
f <- function(x)
{
  # x is a binary vector, if it 'selects' less than 2 or more than 1000
  # attributes, automatically return 0, because of the hard limit
  if (sum(x) < 2 || sum(x) > 1000) {
    f = 0
  } else {
    # Create a temporary vector of integers from 1 to the number of
    # elements in x (which represent attributes).
    s <- seq(1, length(x))
    # Select only those integers where the matching element in x is 1
    p <- s[as.logical(x)]
    # Add 1071, which is the index of our target class, which must always
    # be included for classification
    p <- append(p, 1071, after = length(p))
    
    # Select the chosen columns from the dataset
    d <- select(mydata, p)
    
    # Train the model using the selected data. The target class is
    # is dependent on all of the attributes in the passed data. We
    # use a tree model for classification
    model <- train(class~., data = d, trControl=trn_ctrl, method = 'ctree')
    
    # Calculate the fitness value. Since we are using 3 fold cross validation
    # we average the accuracy calculated on each of the 3 testing sets. We
    # multiply this value with a weight, calculated from the number of
    # selected attributes.
    f = mean(model$results$Accuracy) * w(sum(x))
  }
}

# Start the genetic algorithm. Use binary vector specimens and the fitness
# function 'f'. nBits is set to 1070, since this is the number of attributes
# we can choose from.
GA <- ga(type = 'binary',
         fitness = f,
         nBits = 1070,
         maxiter = 1000,
         run = 1000,
         popSize = 50,
         parallel = TRUE)
save(GA, file = './result.RData')
summary(GA)

# Convert the class from string values to a simple binary representation
class <- 1 * (mydata[, 1071] == 'DLBCL')

# Calculate the corelation
corelation <- cor(x = mydata[,1:1070], y = class)

# Sort the vector and get the appropriate value
val <- (sort(corelation, decreasing = TRUE))[sum(GA@solution[1, ])]

# Calculate indices that are higher than the value
result <- (corelation >= val) * 1

# Count the number of matching attributes
matching <- sum(GA@solution[1, ] * result)