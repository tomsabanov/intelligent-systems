library(tm)
library(SnowballC)
library(RColorBrewer)
library(cluster)
library(fpc)

# Exploration


######################### Frequent Terms
rm(list = ls())
source("cleanData.R")
data <- cleanData("./data/train.tsv")
corpus <- data[[1]]
corpus.POS <-data[[2]]
corpus.labels <- data[[3]]


# Create term-document matrix
tdm <- TermDocumentMatrix(corpus)
tdm <- removeSparseTerms(tdm, 0.995)

# 10918 terms, 2220 documents, extremely sparse, more than 99% entries being zero
# Ignore/Remove hashed proper nouns?
tdm

m <- as.matrix(tdm)
v <- sort(rowSums(m),decreasing=TRUE)
d <- data.frame(word = names(v),freq=v)
head(d, 20)

library(wordcloud)
set.seed(1234)
wordcloud(words = d$word, freq = d$freq, min.freq = 1,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))


# Inspect frequent words (with frequency no less than 100)
findFreqTerms(tdm, lowfreq=50)

termFrequency <- rowSums(as.matrix(tdm))
termFrequency <- subset(termFrequency, termFrequency >= 100)
qplot(seq(length(termFrequency)),sort(termFrequency), xlab = "index", ylab = "Freq")

barplot(d[1:10,]$freq, las = 2, names.arg = d[1:10,]$word,
        col ="lightblue", main ="Most frequent words",
        ylab = "Word frequencies")

########################

######################## Clustering for cluster number k {2,4,8,16} on
####################### word2vec based and non-neural text representations

kmeans_dim <- function(corpus, k){
  
  # Constructs a document-term matrix
  dtm <- DocumentTermMatrix(corpus, control = list(weighting=weightTfIdf))
  dtm <- removeSparseTerms(dtm, 0.995)
  mat <- as.matrix(dtm)

  # Cluster the documents using the kmeans method with the number of clusters set to k
  kmeansResult <- kmeans(mat, centers=k)

  # Find the most popular words in every cluster
  for (i in 1:k) 
  {
    s <- sort(kmeansResult$centers[i,], decreasing=T)
    cat(names(s)[1:5], "\n")
  }
}
kmeans_dim(corpus,16)

#######################

####################### Project via PCA or t-SNE
library(ggplot2)

data <- cleanData("./data/train.tsv")
corpus <- data[[1]]
corpus.POS <-data[[2]]
corpus.labels <- data[[3]]

dtm <- DocumentTermMatrix(corpus, control = list(weighting=weightTfIdf))
dtm <- removeSparseTerms(tfidf, 0.995)

vis_pca <- function(dtm,k){
  mat <- as.matrix(dtm)
  pca <- prcomp(mat, rank. = 2)
  comp1 <- as.numeric(pca$x[,1])
  comp2 <- as.numeric(pca$x[,2])
  p <- cbind(comp1, comp2)
  cluster <- kmeans(p, centers = k)
  plot(comp1, comp2, col=cluster$cluster)
}
vis_pca(dtm,16)

######################## POS tagging
source("cleanData.R")

data <- cleanData("./data/train.tsv")
corpus <- data[[1]]
corpus.POS <-data[[2]]
corpus.labels <- data[[3]]

corpus.labels
sum(corpus.labels)

content(corpus[2])
corpus.POS[2]
corpus.labels[2]

############################################################################################################### 3. 

############### Discuss how balanced is the class label distribution?


############### Select at least 2 classification models and discuss their choice



############## Select hyperparameter tuning method of choice and find the best representation-model configurations using the sufficiently split train.tsv (use e.g., cross validation)
rm(list = ls())
source("cleanData.R")

data <- cleanData("./data/train.tsv")
corpus <- data[[1]]
corpus.POS <-data[[2]]
corpus.labels <- data[[3]]

testData <- cleanData("./data/test.tsv")
corpus_test <- testData[[1]]
corpus_test.POS <-testData[[2]]
corpus_test.labels <- testData[[3]]

#train data
trainData.tfidf <- DocumentTermMatrix(corpus, control = list(weighting=weightTfIdf))
trainSparse = removeSparseTerms(trainData.tfidf, 0.995)
trainData.mat <- as.matrix(trainSparse)
trainDataSet <- cbind(trainData.mat, corpus.labels)
trainDataSet$Insult <- as.factor(trainDataSet$Insult)
dim(trainDataSet)

#test data
testData.tfidf <- DocumentTermMatrix(corpus_test, control = list(dictionary=Terms(trainSparse),weighting=weightTfIdf))
# remove empty documents
rowTotals <- (apply(testData.tfidf, 1, sum) > 0) #Find the sum of words in each Document
corpus_test <- corpus_test[rowTotals]
Terms(trainSparse)

testData.tfidf <- DocumentTermMatrix(corpus_test, control = list(dictionary=Terms(trainSparse),weighting=weightTfIdf))
corpus_test.labels <- corpus_test.labels[rowTotals,] #remove labels too
corpus_test.labels <- as.data.frame(corpus_test.labels)
names(corpus_test.labels)[names(corpus_test.labels) == "corpus_test.labels"] <- "Insult"

testData.mat <- as.matrix(testData.tfidf)
testDataSet <- cbind(testData.mat, corpus_test.labels)
testDataSet$Insult <- as.factor(testDataSet$Insult)

colnames(testDataSet)
colnames(trainDataSet)

#
# Document classification 
#

# Cross validation, KNN
library(caret)
set.seed(1000)
trControl <- trainControl(method  = "cv",number  = 3)
fit <- train(Insult ~ .,
             method     = "knn",
             tuneGrid   = expand.grid(k = 1:20),
             trControl  = trControl,
             preProcess = c("center","scale"), 
             metric     = "Accuracy",
             data       = trainDataSet)
plot(fit)
fit

observed <- testDataSet$Insult
knnPredict <- predict(fit,newdata = testDataSet )
confusionMatrix(knnPredict, testDataSet$Insult )

t <- table(observed, knnPredict)
t
# Classification accuracy
sum(diag(t))/sum(t)
# Recall (here calculated for the class "general")
t[1,1]/sum(t[1,])
# Precision (here calculated for the class "general")
t[1,1]/sum(t[,1])


##### SVM
library(caret)
set.seed(1000)
control <- trainControl(method="repeatedcv", number=3, repeats=3)
fit <- train(Insult ~ .,
                data = testDataSet,
                method = "svmRadial",
                preProcess = c("center", "scale"),
                tuneLength = 10,
                trControl = control)
plot(fit)
fit

observed <- testDataSet$Insult
knnPredict <- predict(fit,newdata = testDataSet )
confusionMatrix(knnPredict, testDataSet$Insult )
t <- table(observed, knnPredict)
t
# Classification accuracy
sum(diag(t))/sum(t)
# Recall (here calculated for the class "general")
t[1,1]/sum(t[1,])
# Precision (here calculated for the class "general")
t[1,1]/sum(t[,1])




##################################### POS tag-based representations 


