library(ggplot2)
library(caret)
detach("package:caret",unload=TRUE)
detach("package:ggplot2", unload=TRUE)
library(NLP)
library(openNLP)
library(tm)
library(fpc)
library(stringr)
library(digest)
library(openssl)
#https://reaktanz.de/R/pckg/koRpus/koRpus_vignette.html#treetagger

cleanData <- function(fileName){
  data <- read.table(
    file = fileName,
    sep = '\t',
    quote = "", # THIS IS IMPORTANT
    header = TRUE,
    fileEncoding="UTF-8",
    stringsAsFactors = FALSE # ALSO IMPORTANT
  )
  
  # Get all data from the file
  text <- data[,2]
  length(text)

  # 1.  Get rid of special characters and unknown symbols
  text <- gsub("\\\\\\\\'","'",text) # keep apostrophees
  rgx = "[^a-zA-Z0-9']|\\\\t|\\\\n|\\\\\\\\n|\\\\x[0-9a-f]{2}|\\\\\\\\x[0-9a-f]{2}|<[^\\>]+>|\"|\\\\\\\\|\\\\u[0-9]{4}"
  cleanedText <- gsub(rgx, " ", text)
  cleanedText <- gsub("^\\s+|\\s+$", "", cleanedText) # remove leading and trailing white spaces
  
  # Construct a corpus for a vector as input.
  corpus <- Corpus(VectorSource(cleanedText))  
  
  # 2. Remove stopwords
  # Read the custom stopwords list
  conn = file("english.stop.txt", open="r")
  mystopwords = readLines(conn)
  close(conn)
  corpus <- tm_map(corpus, removeWords, mystopwords)

  # 3. Remove punctuations
  corpus <- tm_map(corpus, removePunctuation)
  # Strip extra whitespace from text documents
  corpus <- tm_map(corpus, stripWhitespace)
  # Stemming
  #corpus <- tm_map(corpus, stemDocument,"english")
  
    # Remove empty documents
  dtm <- DocumentTermMatrix(corpus, control = list(weighting=weightTfIdf))
  a0 = (apply(dtm, 1, sum) > 0)   # build vector to identify non-empty docs
  dtm = dtm[a0,]      # drop empty docs
  corpus <- corpus[a0]

  labels <-list(data[,1])
  labels <- labels[[1]]
  labels <- labels[a0]

  # 4. Generate document representations comprised of part-of-speech-tags (POS)
  corpus_vec <- lapply(corpus , function(x){
    x <- as.String(x)
    x <- gsub("^\\s+|\\s+$", "", x) # remove leading and trailing white spaces
  })

  sent_token_annotator  <- Maxent_Sent_Token_Annotator ()
  word_token_annotator  <- Maxent_Word_Token_Annotator ()
  pos_tag_annotator  <- Maxent_POS_Tag_Annotator ()
    
  hashed_corpus <<- list()
  i <<- 1
  corpus.POS <- lapply(corpus_vec,function(x){
    y1 <- annotate(x, list(sent_token_annotator ,word_token_annotator))
    y2 <- annotate(x, pos_tag_annotator , y1)
    
    y2w <- subset(y2,type == "word")
    tags <- sapply(y2w$features, '[[',"POS")
    
    # 5. Anonymize Proper nouns
    hashes <- which(tags %in% "NNP")
    words <- strsplit(as.String(x),' ')
    words <- words[[1]]
    
    for(hash in hashes){
      #words[hash] <- md5(as.String(words[hash])) # Remove proper nouns
      words[hash] <- as.String("")
    }

    hashed_corpus[i] <<- paste(words, collapse=" ")
    
    i <<- i + 1
    
    return(tags)
  })
  
  corpus <- Corpus(VectorSource(hashed_corpus))
  corpus <- tm_map(corpus, tolower)
  corpus <- tm_map(corpus, stripWhitespace)

  corpus.POS <- corpus.POS
  corpus.labels <- labels
  
  corpus.labels <- c(corpus.labels)
  corpus.labels <- data.frame(as.table(corpus.labels))
  corpus.labels <- corpus.labels["Freq"]
  names(corpus.labels)[ncol(corpus.labels)] <- "Insult"
  

  
  return(list(corpus,corpus.POS,corpus.labels)) 
}
